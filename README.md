# Zadanie_rekr

### About 

This application is designed to help searching for rental flats in the area chosen by the user. One can adjust their search by using filters (adjusting price range, number of bedrooms and policies). 

I have used following technologies to build this project: 

* Vue.js(Nuxt.js)
* Vuex 

I am also using Javascript library - Mapboxgl - to draw the map. 

### Feed with flats to display

I am using JSON file to display flats. If you wish to add flats to the feed, you can do so by adding objects to feed.json file. Make sure that they include following properties: 

* _id: string
* name: string
* address: string
* price: float(00.00)
* longitude: float
* latitude: float
* bedrooms: number
* bthrooms: number

### Cross-browser compatibility 

The application has been tested on Chrome v.80 and Mozilla Firefox v.72.


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
