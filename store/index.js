import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

const state = () => ({
    map: undefined,
    markers: [],
    feed: [],
    results: {},
    filtered: {
        range: [1000, 4000],
        bedroomCount: 2,
        bathroomCount: 1,
        smoking: false,
        pets: false,
        section: false
    },
    themeColor: "#5e4da9",
})

const mutations = {
    setMap(state, map) {
        state.map = map;
    },
    setMarkers(state, markers) {
        state.markers = markers;
    },
    setFeed(state, json) {
        state.feed = json;
    },
    setResults(state, json) {
        state.results = json;
    },
    increaseBed(state) {
        state.filtered.bedroomCount++;
    },
    decreaseBed(state) {
        state.filtered.bedroomCount--;
    },
    increaseBath(state) {
        state.filtered.bathroomCount++;
    },
    decreaseBath(state) {
        state.filtered.bathroomCount--;
    },
    setRange(state, range) {
        state.filtered.range = range;
    },
    setSmoking(state, switchValue) {
        state.filtered.smoking = switchValue;
    },
    setPets(state, switchValue) {
        state.filtered.pets = switchValue;
    },
    setSection(state, switchValue) {
        state.filtered.section = switchValue;
    }
}

const actions = {
    createMap(context) {
        return new Promise((resolve) => {
            const mapboxgl = require('mapbox-gl/dist/mapbox-gl');
    
            mapboxgl.accessToken = 'pk.eyJ1IjoiYWdhdGFwIiwiYSI6ImNrOGtuZGVuMTAyeDczbW8xNHo4ejF4NzAifQ.yDOT0EXP5H3XDfcQIdVmUg';
            var map =  new mapboxgl.Map({
                container: 'map-container',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [-73.980641, 40.742208],
                zoom: 10
            })
            context.commit('setMap', map);
            resolve({success: true});
        })
    },
    createMarkers(context) {
        let markers = [];
        const mapboxgl = require('mapbox-gl/dist/mapbox-gl');
        context.state.feed.forEach(element => {
            let marker = document.createElement('div');
            marker.style.width = "65px";
            marker.style.height = "30px";
            marker.style.display = 'flex';
            marker.style.justifyContent = "center";
            marker.style.alignItems = "center";
            marker.style.borderRadius = "5%";
            marker.style.backgroundColor = context.state.themeColor;
            marker.style.padding = "5px";
            marker.style.transition = "width 1s, height 1s";
            marker.id = element._id;
            let markerPrice = document.createElement("div");
            markerPrice.style.color = "white";
            markerPrice.style.fontWeight = "700";
            markerPrice.innerHTML = `$${element.price}`;
            let markerPriceAfter = document.createElement("div");
            markerPriceAfter.style.width = "15px";
            markerPriceAfter.style.height = "15px";
            markerPriceAfter.style.backgroundColor = context.state.themeColor;
            markerPriceAfter.style.position = "absolute";
            markerPriceAfter.style.bottom = "-7px";
            markerPriceAfter.style.left = "50%";
            markerPriceAfter.style.transform = "translateX(-50%)";
            markerPriceAfter.style.transform = "rotate(45deg)";
            markerPriceAfter.style.zIndex = "-3";
            marker.appendChild(markerPrice);
            marker.appendChild(markerPriceAfter);
            markers.push(marker);
            new mapboxgl.Marker(marker).setLngLat([element.longitude, element.latitude]).addTo(context.state.map);
        });
        context.commit('setMarkers', markers);
    },
    highlightMarker(context, id) {
        context.state.markers.map(function(item) {
            if(item.id != id && item.style.width != "65px") {
                item.style.width = "65px";
                item.style.height = "30px";
                item.style.boxShadow = "";
            } else if(item.id == id) {
                item.style.width = "75px";
                item.style.height = "40px";
                item.style.boxShadow = "0px -2px 13px 6px rgba(184,109,166,1)";
            } else return '';
        })
    },
    getFeed(context) {
        return new Promise((resolve) => {
            fetch('feed.json').then(resp => {
                return resp.json();
            }).then(json => {
                context.commit('setFeed', json);
                context.commit('setResults', json);
                resolve({success: true});
            })
        })
    },
    filterPrice(context) {
        let filteredFeed = [];
        context.state.results.map(function(item) {
            if(item.price >= context.state.filtered.range[0] && item.price <= context.state.filtered.range[1]) {
                filteredFeed.push(item);
            } else return '';  
        });
        context.commit('setResults', filteredFeed);
    },
    filterBed(context) {
        let filteredFeed = [];
        context.state.results.map(function(item) {
            if(item.bedrooms == context.state.filtered.bedroomCount && item.bathrooms == context.state.filtered.bathroomCount) {
                filteredFeed.push(item);
            } else return '';
        });
        context.commit('setResults', filteredFeed);
    },
    filterPolicies(context) {
        let filteredFeed = [];
        context.state.results.map(function(item) {
            if(context.state.filtered.smoking && item.tags.includes("smokingAllowed")) {
                filteredFeed.push(item);
            } else if(context.state.filtered.pets && item.tags.includes("petsAllowed")) {
                filteredFeed.push(item);
            } else if(context.state.filtered.section && item.tags.includes("sectionAndHousing")) {
                filteredFeed.push(item);
            } else return '';
        });
        context.commit('setResults', filteredFeed);
    },
    resetFilter(context) {
        let feed = context.state.feed;
        context.commit('setResults', feed);
    },
    searchByAddress(context, value) {
        let searchResult = [];
        context.state.feed.map(function(item) {
            if(item.address.includes(value)) {
                searchResult.push(item);
            } else return '';
        });
        context.commit('setResults', searchResult);
    }
}

const getters = {
    getMap(state) {
        return state.map
    }
}

export default () => new Vuex.Store({
    state,
    mutations,
    actions,
    getters
  })

